import Vue from "vue"
import Vuex from "vuex"
import axios from "axios"
import VueAxios from "vue-axios"

Vue.use(Vuex)
Vue.use(VueAxios, axios)
Vue.axios.defaults.baseURL = 'https://pokeapi.co/api/v2/pokemon/'

const state = {
    pokemons: [],
    pokemon: {},
}

const getters = {
    pokemons: state => state.pokemons,
    pokemon: state => state.pokemon,
}

const mutations = {
    SAVE_POKEMONS: (state, pokemons) => {
        state.pokemons = pokemons
    },
    SAVE_POKEMON: (state, pokemon) => {
        state.pokemon = pokemon
    }
}

const actions = {

    async loadPokemons({commit}) {
        let pokemons = []
        for (let i = 1; i < 21; i++) {
             try { 
                let result  = await Vue.axios.get(`${i}`) 
                pokemons.push(result.data)
            }
            catch(error) { 
                throw new Error(`API ${error}`)
            }
        }
        commit('SAVE_POKEMONS', pokemons)  
    },
    
    async loadPokemonByName({commit}, name) {
        try {
            let result = await Vue.axios.get(`${name}`)
            commit('SAVE_POKEMON', result.data)
        }
        catch(error) {
            throw new Error(`API ${error}`)
        }
    }

}

const pokemonStore = {
    state,
    getters,
    mutations,
    actions
}
export default pokemonStore