import styled from 'vue-styled-components'

const propsInput = {

}

const InputSearch = styled('input', propsInput)`
    border-radius: 5px;
    border: 1px solid #2c3e50;
    color: #2c3e50;
    padding: 1.5rem;
    font-size: 20px;
    -webkit-box-shadow: inset 5px 5px 5px 0px rgba(0,0,0,0.17);
    -moz-box-shadow: inset 5px 5px 5px 0px rgba(0,0,0,0.17);
    box-shadow: inset 5px 5px 5px 0px rgba(0,0,0,0.17);

`
export default InputSearch