import styled from 'vue-styled-components'

const propsTitle = {

}

const Title = styled('h1', propsTitle)`
    text-align: center;
    font-family: 'Roboto', sans-serif;
    font-size: 5rem;
    color: #2c3e50;
    text-transform: capitalize;

`
export default Title