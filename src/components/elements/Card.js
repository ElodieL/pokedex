import styled from 'vue-styled-components'

const propsCard = {

}

const Card = styled('div', propsCard)`
    width: 150px;
    border-radius: 5px;
    border: 1px solid #2c3e50;
    padding: 15px;
    margin: 0.5rem;
    display: flex;
    flex-direction: column;
    align-items: center;
    font-family: 'Roboto', sans-serif;
    color: #2c3e50;
    -webkit-box-shadow: 7px 7px 5px 0px rgba(0,0,0,0.17);
    -moz-box-shadow: 7px 7px 5px 0px rgba(0,0,0,0.17);
    box-shadow: 7px 7px 5px 0px rgba(0,0,0,0.17);

    a {
        text-decoration: none;
        color: #2c3e50;
    }
    a:hover 
    {
        text-decoration:none; 
        cursor:pointer; 
    }

    h2 {
        text-transform: capitalize;
    }

`
export default Card