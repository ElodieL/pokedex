import styled, { css } from 'vue-styled-components'


const propsContainer = { 
    flex: Boolean,
    row: Boolean,
    column: Boolean,
    center: Boolean,
    justifyCenter: Boolean,
    spaceAround: Boolean,
    pokeList: Boolean,
    pokeDetails: Boolean,
    pokeInfo: Boolean
}

const Container = styled('div', propsContainer)`
    margin-left: 10%;
    margin-right: 10%;
    margin-top: 30px;

    ${props => props.pokeList && css`
        margin-left: 15px;
        margin-right: 15px;
    `}

    ${props => props.pokeDetails && css`
        div {
            padding: 15px;
            margin: 1.5rem;
            font-family: 'Roboto', sans-serif;
            font-size: 1.5rem;            
            
            h2 {
                text-align: center;
                font-weight: bold;
                margin: 5px;
            }

            img {
                heigth: 200px;
                width: 200px;
            }
        }
    `}

    ${props => props.pokeInfo && css`
        -webkit-box-shadow: 7px 7px 5px 0px rgba(0,0,0,0.17);
        -moz-box-shadow: 7px 7px 5px 0px rgba(0,0,0,0.17);
        box-shadow: 7px 7px 5px 0px rgba(0,0,0,0.17);
        border-radius: 5px;
        border: 1px solid #2c3e50;
    `}
    
    ${ props => props.flex && css`
        display: flex;
        flex-wrap: wrap;

        ${props => props.row && css`
            flex-direction: row;
        `}
        ${props => props.column && css`
            flex-direction: column;
        `}

        ${props => props.alignCenter && css`
            align-items: center;
        `}

        ${props => props.justifyCenter && css`
            justify-content: center;
        `}

        ${props => props.spaceAround && css`
            justify-content: space-around;
        `}
    `}
`

export default Container