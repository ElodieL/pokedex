import Vue from 'vue'
import VueRouter from 'vue-router'
import Pokemons from './pages/Pokemons'
import Pokemon from './pages/Pokemon'
import NotFound from './pages/NotFound'

Vue.use(VueRouter)

const routes = [
    { path: '/', component: Pokemons },
    { path: '/:name', component: Pokemon },
    { path: '*', component: NotFound },
    
    
]

const router = new VueRouter({
    mode: 'history',
    routes
})

export default router
