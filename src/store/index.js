import Vue from 'vue'
import Vuex from 'vuex'
import  pokemonStore from './modules/pokemonStore'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
      pokemonStore
  },
  strict: true,
  plugins: []
})

global.store = store
export default store